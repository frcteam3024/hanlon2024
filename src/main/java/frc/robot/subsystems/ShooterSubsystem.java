// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.ShooterConstants.*;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkLowLevel.MotorType;

public class ShooterSubsystem extends SubsystemBase {
  private final CANSparkMax shooterMotor1;
  private final CANSparkMax shooterMotor2;
  
  public ShooterSubsystem() {
    this.shooterMotor1 = new CANSparkMax(SHOOTER_MOTOR_PORT_1, MotorType.kBrushless);
    this.shooterMotor2 = new CANSparkMax(SHOOTER_MOTOR_PORT_2, MotorType.kBrushless);//doing "this." is not needed
    //may want to consider setting idle mode
    
  }

  public void runShooter(double speed1, double speed2) {
    shooterMotor1.set(speed1);
    shooterMotor2.set(speed2);
  }

  public void stopShooter() {
    shooterMotor1.set(NO_OUTPUT);
    shooterMotor2.set(NO_OUTPUT);
  }
}
