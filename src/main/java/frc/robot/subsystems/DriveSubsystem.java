package frc.robot.subsystems;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInLayouts;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardLayout;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import frc.robot.SwerveModule;
import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.ModuleConstants.*;
import static java.lang.Math.*;

import java.util.function.Supplier;

import com.kauailabs.navx.frc.AHRS;

public class DriveSubsystem extends SubsystemBase {
  private SwerveDriveOdometry odometry;
  private PIDController pidDrive;
  private PIDController pidTurn;
  ShuffleboardTab sbMain = Shuffleboard.getTab("Main");
  ShuffleboardLayout turnPIDLayout = sbMain
  .getLayout(
      "Turn PID",
      BuiltInLayouts.kList
    )
    .withSize(
      1, 
      2
    )
    .withPosition(
      0,
      0
    );
  private ShuffleboardLayout drivePIDLayout = sbMain
    .getLayout(
      "Drive PID",
      BuiltInLayouts.kList
    )
    .withSize(
      1,
      2
    )
    .withPosition(
      1,
      0
    );
  
  //private GenericEntry turnPIDEntry;
  private GenericEntry enablePID;
  private GenericEntry turnP;
  private GenericEntry turnI;
  private GenericEntry turnD;
  private GenericEntry driveP;
  private GenericEntry driveI;
  private GenericEntry driveD;
  private GenericEntry turnOffsetX;
  private GenericEntry turnOffsetY;

  private final SwerveModule frontLeftModule;

  private final SwerveModule frontRightModule;

  private final SwerveModule backLeftModule;

  private final SwerveModule backRightModule;

  private final SwerveModule[] allSwerveModules;

  private final AHRS gyro = new AHRS(SPI.Port.kMXP);
  private double inclineOffset;

  public DriveSubsystem() {
    shuffleboard();

    frontLeftModule = new SwerveModule(
      FL_DRIVE_MOTOR_PORT,
      FL_TURN_MOTOR_PORT,
      FL_TURN_ENCODER_PORT,
      FL_ENCODER_OFFSET_DEG,
      FL_DRIVE_MOTOR_REVERSED,
      FL_TURN_MOTOR_REVERSED,
      FL_TURN_ENCODER_REVERSED,
      () -> enablePID,
      () -> driveP.getDouble(0), () -> driveI.getDouble(0), () -> driveD.getDouble(0),
      () -> turnP.getDouble(0), () -> turnI.getDouble(0), () -> turnD.getDouble(0)
    );
    
    frontRightModule = new SwerveModule(
      FR_DRIVE_MOTOR_PORT,
      FR_TURN_MOTOR_PORT,
      FR_TURN_ENCODER_PORT,
      FR_ENCODER_OFFSET_DEG,
      FR_DRIVE_MOTOR_REVERSED,
      FR_TURN_MOTOR_REVERSED,
      FR_TURN_ENCODER_REVERSED,
      () -> enablePID,
      () -> driveP.getDouble(0), () -> driveI.getDouble(0), () -> driveD.getDouble(0),
      () -> turnP.getDouble(0), () -> turnI.getDouble(0), () -> turnD.getDouble(0)
    );
    
    backLeftModule = new SwerveModule(
      BL_DRIVE_MOTOR_PORT,
      BL_TURN_MOTOR_PORT,
      BL_TURN_ENCODER_PORT,
      BL_ENCODER_OFFSET_DEG,
      BL_DRIVE_MOTOR_REVERSED,
      BL_TURN_MOTOR_REVERSED,
      BL_TURN_ENCODER_REVERSED,
      () -> enablePID,
      () -> driveP.getDouble(0), () -> driveI.getDouble(0), () -> driveD.getDouble(0),
      () -> turnP.getDouble(0), () -> turnI.getDouble(0), () -> turnD.getDouble(0)
    );

    backRightModule = new SwerveModule(
      BR_DRIVE_MOTOR_PORT,
      BR_TURN_MOTOR_PORT,
      BR_TURN_ENCODER_PORT,
      BR_ENCODER_OFFSET_DEG,
      BR_DRIVE_MOTOR_REVERSED,
      BR_TURN_MOTOR_REVERSED,
      BR_TURN_ENCODER_REVERSED,
      () -> enablePID,
      () -> driveP.getDouble(0), () -> driveI.getDouble(0), () -> driveD.getDouble(0),
      () -> turnP.getDouble(0), () -> turnI.getDouble(0), () -> turnD.getDouble(0)
    );
    
    allSwerveModules = new SwerveModule[]{
      frontLeftModule,
      frontRightModule,
      backLeftModule,
      backRightModule
    };

    this.pidDrive = new PIDController(
      driveP.getDouble(KP_DRIVE), driveI.getDouble(KI_DRIVE), driveD.getDouble(KD_DRIVE));
    this.pidTurn = new PIDController(
      turnP.getDouble(KP_TURN), turnI.getDouble(KI_TURN), turnD.getDouble(KD_TURN));
    this.pidTurn.enableContinuousInput(0, 360);

    // allow 1 sec for gyro to boot up. on a separate thread so other
    // functions can continue to run
    new Thread(() -> {
      try {
        Thread.sleep(1000);
        resetGyro();
        inclineOffset = gyro.getRoll();
      } catch (InterruptedException e) {
        e.printStackTrace();
        Thread.currentThread().interrupt();
      }
    }).start();

    odometry = new SwerveDriveOdometry(DRIVE_KINEMATICS, getGyroYawAsRotation2d(), getModulePositions());
  }

  public double getGyroYaw() {
    return Math.IEEEremainder(gyro.getAngle(), 360);
  }

  /*private double getGyroInclineFromRoll() {
    return inclineOffset - gyro.getRoll();
  }*/

  public Rotation2d getGyroYawAsRotation2d() {
    return Rotation2d.fromDegrees(getGyroYaw());
  }

  public void resetGyro() {
    gyro.reset();
  }
  
  public void brakeMode() {
    for(SwerveModule module : allSwerveModules) {
      module.brakeMode();
    }
  }

  public void coastMode(){
    for(SwerveModule module : allSwerveModules){
      module.coastMode();
    }
  }

  public void stopMotors(){
    for(SwerveModule module : allSwerveModules){
      module.stopMotors();
    }
  }
  
  public void updatePID() {
    for (int i = 0; i < allSwerveModules.length; i++) {
      allSwerveModules[i].updatePID();
    }
  }

  /**
   * @param desiredMovement
   * Set swerve module motors to the outputs needed to obtain the desired movement.
   */
  public void setModules(ChassisSpeeds desiredMovement, Double stickX, Double stickY) {
    SwerveModuleState[] desiredStates = DRIVE_KINEMATICS.toSwerveModuleStates(
      desiredMovement.plus(
        new ChassisSpeeds(
          0.0 , 
          0.0, 
          (turnOffsetX.getDouble(0)*stickX) + (turnOffsetY.getDouble(0)*stickY)
        )
      )
    );
    for (int i = 0; i < allSwerveModules.length; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }
  public void setModules(ChassisSpeeds desiredMovement) {
    SwerveModuleState[] desiredStates = DRIVE_KINEMATICS.toSwerveModuleStates(desiredMovement);
    for (int i = 0; i < allSwerveModules.length; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }
  public void setModules(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < allSwerveModules.length; i++){
      SwerveModuleState desiredState = desiredStates[i];
      allSwerveModules[i].setModuleState(desiredState);
    }
  }

  public SwerveModulePosition[] getModulePositions() {
    SwerveModulePosition[] positions = new SwerveModulePosition[4];
    for (int i = 0; i < allSwerveModules.length; i++) {
        positions[i] = allSwerveModules[i].getModulePosition();
    }
    return positions;
}

  public SwerveModuleState[] getModuleStates() {
    SwerveModuleState[] states = new SwerveModuleState[4];
    for (int i = 0; i < allSwerveModules.length; i++) {
      states[i] = allSwerveModules[i].getModuleState();
    }
    return states;
  }

  public void setModuleStates(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < allSwerveModules.length; i++) {
      allSwerveModules[i].setModuleState(desiredStates[i]);
    }
  }

  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }

  public void resetPose(Pose2d pose) {
    odometry.resetPosition(getGyroYawAsRotation2d(), getModulePositions(), pose);
  }

  public ChassisSpeeds getSpeeds() {
    return DRIVE_KINEMATICS.toChassisSpeeds(getModuleStates());
  }
  
  public void shuffleboard() {
    /*sbMain.addBoolean("PID Drive Status", () -> enablePID);
    sbMain.add("PID Drive Toggle", new InstantCommand(() -> togglePID()));*/
    enablePID = sbMain.add("PID Status", true).withWidget(
      BuiltInWidgets.kToggleButton).withPosition(0, 2).getEntry();
    driveP = drivePIDLayout.addPersistent("Drive P", KP_TURN).getEntry();
    driveI = drivePIDLayout.addPersistent("Drive I", KI_TURN).getEntry();
    driveD = drivePIDLayout.addPersistent("Drive D", KD_TURN).getEntry();
    turnP = turnPIDLayout.addPersistent("Turn P", KP_TURN).getEntry();
    turnI = turnPIDLayout.addPersistent("Turn I", KI_TURN).getEntry();
    turnD = turnPIDLayout.addPersistent("Turn D", KD_TURN).getEntry();
    turnOffsetX = sbMain.addPersistent("Turn Offset for X", 0.0).withWidget(
      BuiltInWidgets.kNumberSlider).withPosition(0, 3).getEntry();
    turnOffsetY = sbMain.addPersistent("Turn Offset for Y", 0.0).withWidget(
      BuiltInWidgets.kNumberSlider).withPosition(0, 3).getEntry();
    sbMain.add("Update PID", new InstantCommand(() -> updatePID()));
    //sbMain.add("Turn PID", pidTurn).withWidget(BuiltInWidgets.kPIDController);
    //sbMain.add("Drive PID", pidDrive).withWidget(BuiltInWidgets.kPIDController);
  }

  /**
   * Checks if the current swerve modules closely enough match a set of desired states.
   * @param desiredStates
   * Arrary of desired states to compare against.
   * @return
   * true if the modules closely enough match the desired states, false if not.
   */
  public boolean checkStates(SwerveModuleState[] desiredStates) {
    for (int i = 0; i < allSwerveModules.length; i++) {
      if (abs(allSwerveModules[i].getTurnInDegrees() - desiredStates[i].angle.getDegrees()) > ANGLE_TOLERANCE_THRESHOLD) {
        return false;
      }
    }
    return true;
  }

  public void resetPID() {
    pidDrive.reset();
    pidTurn.reset();
  }
}
