package frc.robot.subsystems;
import static frc.robot.Constants.VisionConstants.*;
import static java.lang.Math.*;

import java.util.Map;

import org.photonvision.PhotonCamera;
import org.photonvision.PhotonUtils;
// import org.photonvision.PhotonPoseEstimator;
import org.photonvision.targeting.PhotonTrackedTarget;
import org.photonvision.targeting.proto.PhotonTrackedTargetProto;

//import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.shuffleboard.ComplexWidget;
// import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;

public class VisionSubsystem {
  private static final PhotonCamera camera = new PhotonCamera(CAMERA_NAME);

  private final ComplexWidget streamInput;
  private final ComplexWidget streamOutput;

  private boolean flipped = false;
  //private final PhotonPoseEstimator cameraPoseEstimator;  // TODO: subsequent to aftl

  public VisionSubsystem() {
    // TODO: add april tag field layout when released
    // AprilTagFieldLayout aftl = 
    //    new AprilTagFieldLayout(apriltagList, FIELD_LENGTH, FIELD_WIDTH);

    // cameraPoseEstimator = new PhotonPoseEstimator(
    //    aftl, 
    //    PoseStrategy.CLOSEST_TO_REFERENCE_POSE, 
    //    camera,
    //    new Transform3d(ROBOT_TO_CAM, new Rotation3d())
    // );

    camera.setPipelineIndex(APRILTAG_PIPELINE);
    
    ShuffleboardTab sbMain = Shuffleboard.getTab("Main");
    streamInput = sbMain
      .addCamera("Input stream", "input stream", "http://photonvision.local:1181/stream.mjpg")
      .withPosition(2, 0);
    streamOutput = sbMain
      .addCamera("Output stream", "output stream", "http://photonvision.local:1182/stream.mjpg")
      .withPosition(5, 0);
  }
  
  /**
   * @return the best AprilTag ID.
   */
  public int apriltagID() {
    camera.setPipelineIndex(APRILTAG_PIPELINE);
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getFiducialId();
  }

  /**
   * Get a 3D distance between the robot and desired node.
   * @param nodeID the index of the desired node relative to an AprilTag.
   * @return said distance as a 3D vector.
   */

  /**
   * @return camera difference.
   */
  public static Transform3d cameraDiff(int pipe) {
    camera.setPipelineIndex(pipe);
    var result = camera.getLatestResult();
    PhotonTrackedTarget target = result.getBestTarget();
    return target.getBestCameraToTarget();
  }

  /**
   * @return camera difference of specified axis.
   */
  public static double cameraDiffAxis(int pipe, char axis) {
    camera.setPipelineIndex(pipe);
    var diff = cameraDiff(pipe);
    switch(axis) {
      case 'x': return diff.getX();
      case 'y': return diff.getY();
      case 'z': return diff.getZ();
      default:  return 0;
    }
  }

  /** 
   * @return distance of best object.
  */
  public double distanceToObject(int pipe) {
    camera.setPipelineIndex(pipe);
    if (camera.getLatestResult().hasTargets()){
      return PhotonUtils.calculateDistanceToTargetMeters(
        ROBOT_TO_CAM.getZ(), 
        TARGET_HEIGHT, 
        0.0, 
        Units.degreesToRadians(camera.getLatestResult().getBestTarget().getPitch())
      );
    } else {
      return 999;
    }
  }

  /**
   * gets the rotation to a target on a specified axis
   * @param pipe pipeline to use
   * @param axis axis to use (pitch:p, yaw:y)
   * @return rotation. 999 on default
   */
  public double rotationAxisToObject(int pipe, char axis) {
    camera.setPipelineIndex(pipe);
    if (camera.getLatestResult().hasTargets()){
    switch (axis) {
      case 'p': camera.getLatestResult().getBestTarget().getPitch();
      case 'y': camera.getLatestResult().getBestTarget().getYaw();
      default: return 999;
    }
    } else {
      return 999;
    }
  }

  public int getAprilID() {
    camera.setPipelineIndex(APRILTAG_PIPELINE);
    if (camera.getLatestResult().hasTargets()){
      return camera.getLatestResult().getBestTarget().getFiducialId();
    } else {
      return 999;
    }
  }

  public boolean hasTargets() {
    return camera.getLatestResult().hasTargets();
  }

  public void setPipelineIndex(int pipe) {
    camera.setPipelineIndex(pipe);
  }
  
  public void rotateStream() {
    if (!flipped) {
      flipped = true;
      streamInput.withProperties(Map.of("ROTATION", "HALF"));
      streamOutput.withProperties(Map.of("ROTATION", "HALF"));
    }
  }

  public void rotateStreamBack() {
    if (flipped) {
      flipped = false;
      streamInput.withProperties(Map.of("ROTATION", "NONE"));
      streamOutput.withProperties(Map.of("ROTATION", "NONE"));
    }
  }
}