package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.HeadConstants.*;

public class HeadSubsystem extends SubsystemBase{
    private final Servo yawServo;
    private final Servo pitchServo;

    private double yaw = 0.5;
    private double pitch = 0.5;

    public HeadSubsystem() {
        yawServo = new Servo(YAW_SERVO);
        pitchServo = new Servo(PITCH_SERVO);
    }

    public void setYaw(double input) {
        yaw = input;
        yawServo.set(yaw);
    }

    public void setPitch(double input) {
        pitch = input;
        pitchServo.set(pitch);
    }

    public void addToYaw(double input) {
        if (yaw < 0) {
            yaw = 0;
        } else if (yaw > 1) {
            yaw = 1;
        } else {
            yaw += input;
        }
        yawServo.set(yaw);
        System.out.println("yaw: " + yaw);
    }

    public void addToPitch(double input) {
        if (pitch < 0) {
            pitch = 0;
        } else if (pitch > 1) {
            pitch = 1;
        } else {
            pitch += input;
        }
        pitchServo.set(pitch);
        System.out.println("pitch: " + pitch);
    }

    public double getYaw() {
        return yawServo.get();
    }

    public double getPitch() {
        return pitchServo.get();
    }
}
