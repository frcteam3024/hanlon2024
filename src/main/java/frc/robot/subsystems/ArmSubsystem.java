// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.ArmConstants.*;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

public class ArmSubsystem extends SubsystemBase {
  private final VictorSPX armMotor;

  private double spooliage = 0;
  private int timer = 0;

  public ArmSubsystem() {
    this.armMotor = new VictorSPX(ARM_MOTOR_ID);
  }

  public void runArm(double speed) {
    spooliage += speed;
    armMotor.set(ControlMode.PercentOutput, speed);
  }

  public void stopArm() {
    armMotor.set(ControlMode.PercentOutput, NO_OUTPUT);
  }

  public void brakeMode() {
    armMotor.setNeutralMode(NeutralMode.Brake);
  }

  public void coastMode() {
    armMotor.setNeutralMode(NeutralMode.Coast);
  }

  public double getSpooliage() {
    return spooliage;
  }
  
  public int getTimer() {
    return timer;
  }

  public void resetTimer() {
    timer = UNSPOOL_TIMER;
  }

  public void tickTimer() {
    timer--;
  }
}
