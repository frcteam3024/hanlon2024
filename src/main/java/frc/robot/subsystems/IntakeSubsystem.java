// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import static frc.robot.Constants.IntakeConstants.*;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class IntakeSubsystem extends SubsystemBase {
  private final VictorSPX intakeVictor;
  private final CANSparkMax intakeSpark;

  public IntakeSubsystem() {
    this.intakeVictor = new VictorSPX(INTAKE_VICTOR_ID);
    intakeSpark = new CANSparkMax(INTAKE_SPARK_ID, MotorType.kBrushless);
  }

  public void runIntake(double speed) {
    intakeVictor.set(ControlMode.PercentOutput, -speed);
    intakeSpark.set(speed * 0.7);
  }

  public void stopIntake() {
    intakeVictor.set(ControlMode.PercentOutput, NO_OUTPUT);
    intakeSpark.set(NO_OUTPUT);
  }

  public void brakeMode() {
    intakeVictor.setNeutralMode(NeutralMode.Brake);//this does not need to be a method by itsself
    intakeSpark.setIdleMode(IdleMode.kBrake);
  }

  public void coastMode() {
    intakeVictor.setNeutralMode(NeutralMode.Coast);
    intakeSpark.setIdleMode(IdleMode.kCoast);
  }
}
