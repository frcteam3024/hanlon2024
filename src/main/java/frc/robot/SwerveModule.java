// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static frc.robot.Constants.ModuleConstants.*;
import static frc.robot.Constants.DriveConstants.*;
import static java.lang.Math.*;

import java.util.function.Supplier;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkBase.IdleMode;
import com.revrobotics.CANSparkLowLevel.MotorType;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
// import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
// import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.networktables.GenericEntry;
import edu.wpi.first.wpilibj.AnalogEncoder;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;


public class SwerveModule {
    private final CANSparkMax driveMotor;
    private final CANSparkMax turnMotor;
    private final AnalogEncoder turnEncoder;
    private final double encoderOffsetDeg;
    private final boolean turnEncoderReversed;
    private final int channel;
    private final Supplier<GenericEntry> enablePID;
    /*private final Supplier<PIDController> pidDrive;
    private final Supplier<PIDController> pidTurn;*/
    private final Supplier<Double> driveP;
    private final Supplier<Double> driveI;
    private final Supplier<Double> driveD;
    private final Supplier<Double> turnP;
    private final Supplier<Double> turnI;
    private final Supplier<Double> turnD;

    private final PIDController pidDrive;
    private final PIDController pidTurn;
    private final SimpleMotorFeedforward feedforward;

    // private double encoderAngle; // [0,1]
    // private final double encoderOutScalar = 0.1;

    // shuffleboard vars
    private double sbAngleRaw = 0;  // get turn deg
    private double sbAngleDegrees = 0;
    private double sbAngleDegreesRelative = 0;
    private double sbAngleDegreesFlipped = 0;
    private double sbAngleDegreesRemainder = 0;
    private String sbModuleState = "";  // set module state
    private String sbModuleStateOptimized = "";
    private double sbDriveOutput = 0;
    private double sbTurnOutput = 0;
    private double sbTurnEncoder = 0;
    private double sbTargetAngle = 0;   // optimize
    private double sbAngleErrorRaw = 0;
    private double sbAngleErrorOptimized = 0;
    private double sbAngleOptimized = 0;

    /** contains drive and turn motors and all methods to directly interact with them */
    public SwerveModule(
        int driveMotorID,
        int turnMotorID,
        int encoderID,
        double encoderOffsetDeg,
        boolean driveMotorReversed,
        boolean turnMotorReversed,
        boolean turnEncoderReversed,
        Supplier<GenericEntry> enablePID,
        /*Supplier<PIDController> pidDrive,
        Supplier<PIDController> pidTurn*/
        Supplier<Double> driveP, Supplier<Double> driveI, Supplier<Double> driveD,
        Supplier<Double> turnP, Supplier<Double> turnI, Supplier<Double> turnD
    ) {
      this.driveMotor = new CANSparkMax(driveMotorID, MotorType.kBrushless);
      this.driveMotor.setInverted(driveMotorReversed);

      this.turnMotor = new CANSparkMax(turnMotorID, MotorType.kBrushless);
      this.turnMotor.setInverted(turnMotorReversed);

      this.turnEncoder = new AnalogEncoder(encoderID);
      // this.turnEncoder.setDistancePerRotation(2.0 * Math.PI);
      this.turnEncoderReversed = turnEncoderReversed;
      this.encoderOffsetDeg = encoderOffsetDeg;
      // this.encoderAngle = encoderOffsetDeg;

      this.enablePID = enablePID;
      /*this.pidDrive = pidDrive;
      this.pidTurn = pidTurn;*/
      this.driveP = driveP; this.driveI = driveI; this.driveD = driveD;
      this.turnP = turnP; this.turnI = turnI; this.turnD = turnD;

      pidDrive = new PIDController(driveP.get(), driveI.get(), driveD.get());
      pidTurn = new PIDController(turnP.get(), turnI.get(), turnD.get());

      this.feedforward = new SimpleMotorFeedforward(0., 0.);

      this.channel = turnEncoder.getChannel();
    
      shuffleboard();
      resetEncoders();
    }

    /**
     * @return angle in degrees of the turn motor
     */
    public double getTurnInDegrees() {
      // final double rawAngle = encoderAngle;
      final double rawAngle = turnEncoder.getAbsolutePosition();
      sbAngleRaw = rawAngle;
      double degAngle = rawAngle * 360;
      sbAngleDegrees = degAngle;
      degAngle = degAngle - encoderOffsetDeg;
      sbAngleDegreesRelative = degAngle;
      degAngle *= -1;
      sbAngleDegreesFlipped = degAngle;
      degAngle = IEEEremainder(degAngle, 360);
      //degAngle = MathUtil.inputModulus(degAngle, 0, 180);
      sbAngleDegreesRemainder = degAngle;
      return degAngle;
    }

    public Rotation2d getTurnInRotation2d() {
      return Rotation2d.fromDegrees(abs(getTurnInDegrees()));
    }

    /** reset all turn encoders to zero (untested) */
    public void resetEncoders() {
      turnEncoder.reset();
      // encoderAngle = 0;
    }

    /*public void resetPID() {
      turnPID.reset();
      drivePID.reset();
    }*/

    public void updatePID() {
      pidDrive.setPID(driveP.get(), driveI.get(), driveD.get());
      pidTurn.setPID(turnP.get(), turnI.get(), turnD.get());
      System.out.println(pidTurn.getI());
    }

    /**
     * set module motors as described in a given state
     * @param desiredState the state describing the drive motor and turn motor outputs
     */
    public void setModuleState(SwerveModuleState desiredState) {
      sbModuleState = desiredState.toString();
      final double currentAngle = getTurnInDegrees();
      desiredState = optimize(desiredState, currentAngle);
      sbModuleStateOptimized = desiredState.toString();
      final double angleError = desiredState.angle.getDegrees();

      double driveOutput;
      double turnOutput;
      if (enablePID.get().getBoolean(true)) {
        //driveOutput = pidDrive.calculate(
        //  getModuleState().speedMetersPerSecond, desiredState.speedMetersPerSecond) + feedforward.calculate(getModuleState().speedMetersPerSecond);
        driveOutput = desiredState.speedMetersPerSecond / MAX_PHYSICAL_SPEED;
        ///turnOutput = pidTurn.calculate(currentAngle, angleError);
        turnOutput = turnP.get() * angleError;
      } else {
        driveOutput = desiredState.speedMetersPerSecond / MAX_PHYSICAL_SPEED;
        turnOutput = turnP.get() * angleError;
      }

      System.out.println("Turn speed: " + turnOutput + "ID: " + turnMotor);
      
      if (abs(driveOutput) > 1) driveOutput = MAX_DRIVE_SPEED;
      if (abs(driveOutput) < MIN_MOTOR_OUTPUT) driveOutput = NO_OUTPUT;
      if (abs(turnOutput)  < MIN_MOTOR_OUTPUT) turnOutput  = NO_OUTPUT;

      sbDriveOutput = driveOutput;
      sbTurnOutput = turnOutput;
      sbTurnEncoder = currentAngle;

      if (ENABLE_DRIVE_MOTORS) driveMotor.setVoltage(driveOutput * 12);
      else System.out.println("UNABLE TO DRIVE: ENABLE_DRIVE_MOTORS has been set to false. To fix this go to src/main/java/frc/robot/Constants.java and set it to true.");
      if (ENABLE_TURN_MOTORS)  turnMotor.setVoltage(turnOutput * 12);
      else System.out.println("UNABLE TO DRIVE: ENABLE_TURN_MOTORS has been set to false. To fix this go to src/main/java/frc/robot/Constants.java and set it to true.");
      // encoderAngle += turnOutput * encoderOutScalar;
    }

    public SwerveModulePosition getModulePosition() {
      return new SwerveModulePosition();  // TODO: turn encoders
    }

    public SwerveModuleState getModuleState() {
      return new SwerveModuleState(1, getTurnInRotation2d()); // TODO: correct speed meters per second
    }

    /**
     * use a shortest path algorithm to calculate the minumum angle the wheel will have to spin
     * and whether or not the output to the drive motor should be inverted
     * @param desiredState unoptimized state describing target drive and turn motor outputs
     * @param currentAngle current degree angle of the turn motor
     * @return SwerveModuleState with optimized drive and turn motor outputs
     */
    private SwerveModuleState optimize(SwerveModuleState desiredState, double currentAngle) {
      final double targetAngle = desiredState.angle.getDegrees();
      sbTargetAngle = targetAngle;
      final double rawError = targetAngle - currentAngle;
      sbAngleErrorRaw = rawError;
      final double optimizedError = IEEEremainder(rawError, 360);
      sbAngleErrorOptimized = optimizedError;

      final double speed = desiredState.speedMetersPerSecond;
      final double optimizedSpeed;
      final double optimizedAngle;
      if (optimizedError < FLIP_THRESHOLD) {
        optimizedSpeed = speed;
        optimizedAngle = optimizedError;
      } else {
        optimizedSpeed = -speed;
        optimizedAngle = IEEEremainder(optimizedError, 360);
      }
      sbAngleOptimized = optimizedAngle;
      Rotation2d optimizedRotation = new Rotation2d(Units.degreesToRadians(optimizedAngle));
      return new SwerveModuleState(optimizedSpeed, optimizedRotation);
    }

    /** stop the drive and turn motors */
    public void stopMotors() {
      sbDriveOutput = NO_OUTPUT;
      sbTurnOutput = NO_OUTPUT;
      driveMotor.set(NO_OUTPUT);
      turnMotor.set(NO_OUTPUT);
    }

    /** set drive and turn motors to brake mode */
    public void brakeMode() {
      driveMotor.setIdleMode(IdleMode.kBrake);
      turnMotor.setIdleMode(IdleMode.kBrake);
    }

    /** set drive and turn motors to caost mode */
    public void coastMode() {
      driveMotor.setIdleMode(IdleMode.kCoast);
      turnMotor.setIdleMode(IdleMode.kCoast);
    }

    public void shuffleboard() {
      ShuffleboardTab sbDebug = Shuffleboard.getTab("Debug");

      sbDebug.addNumber("Raw on ["+channel+"]", () -> sbAngleRaw);
      sbDebug.addNumber("Degrees on ["+channel+"]", () -> sbAngleDegrees);
      sbDebug.addNumber("Degrees (relative) on ["+channel+"]", () -> sbAngleDegreesRelative);
      sbDebug.addNumber("Degrees (flipped) on ["+channel+"]", () -> sbAngleDegreesFlipped);
      sbDebug.addNumber("Degrees (remainder) ["+channel+"]", () -> sbAngleDegreesRemainder);
      sbDebug.addString("State on ["+channel+"]", () -> sbModuleState);
      sbDebug.addString("Optimized State on ["+channel+"]", () -> sbModuleStateOptimized);
      sbDebug.addNumber("Drive Output on ["+channel+"]", () -> sbDriveOutput);
      sbDebug.addNumber("Turn Output on ["+channel+"]", () -> sbTurnOutput);
      sbDebug.addNumber("Turn Encoder on ["+channel+"]", () -> sbTurnEncoder);
      sbDebug.addNumber("Target Angle on ["+channel+"]", () -> sbTargetAngle);
      sbDebug.addNumber("Angle Error (raw) on ["+channel+"]", () -> sbAngleErrorRaw);
      sbDebug.addNumber("Angle Error (opt.) on ["+channel+"]", () -> sbAngleErrorOptimized);
      sbDebug.addNumber("Optimized Angle on ["+channel+"]", () -> sbAngleOptimized);

      sbDebug.addNumber("Drive P on ["+channel+"]", () -> pidDrive.getP());
      sbDebug.addNumber("Drive I on ["+channel+"]", () -> pidDrive.getI());
      sbDebug.addNumber("Drive D on ["+channel+"]", () -> pidDrive.getD());
      sbDebug.addNumber("Turn P on ["+channel+"]", () -> pidTurn.getP());
      sbDebug.addNumber("Turn I on ["+channel+"]", () -> pidTurn.getI());
      sbDebug.addNumber("Turn D on ["+channel+"]", () -> pidTurn.getD());
      sbDebug.addBoolean("Enable PID on ["+channel+"]", () -> enablePID.get().getBoolean(true));
    }
}
