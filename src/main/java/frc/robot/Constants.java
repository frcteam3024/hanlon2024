// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static java.lang.Math.PI;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.math.util.Units;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */

public final class Constants {
  private Constants() {}

  public static final class ModuleConstants {
    private ModuleConstants() {}
    public static final boolean ENABLE_DRIVE_MOTORS = true;
    public static final boolean ENABLE_TURN_MOTORS = true;

    public static final double KP_TURN = 0.007;
    public static final double KI_TURN = 0.000;
    public static final double KD_TURN = 0.001;

    public static final double KP_DRIVE = 0.05;
    public static final double KI_DRIVE = 0.0;
    public static final double KD_DRIVE = 0.0;

    public static final double ANGLE_TOLERANCE_THRESHOLD = 5; // deg

    public static final double FLIP_THRESHOLD = 120;
    public static final double MIN_MOTOR_OUTPUT = 0.05;

    public static final int FL_DRIVE_MOTOR_PORT = 14;
    public static final int FL_TURN_MOTOR_PORT = 30;
    public static final int FL_TURN_ENCODER_PORT = 0;
    public static final double FL_ENCODER_OFFSET_DEG = -86.16;
    public static final boolean FL_DRIVE_MOTOR_REVERSED = true;
    public static final boolean FL_TURN_MOTOR_REVERSED = false;
    public static final boolean FL_TURN_ENCODER_REVERSED = true;

    public static final int FR_DRIVE_MOTOR_PORT = 32;
    public static final int FR_TURN_MOTOR_PORT = 15;
    public static final int FR_TURN_ENCODER_PORT = 1;
    public static final double FR_ENCODER_OFFSET_DEG = -30;
    public static final boolean FR_DRIVE_MOTOR_REVERSED = false;
    public static final boolean FR_TURN_MOTOR_REVERSED = false;
    public static final boolean FR_TURN_ENCODER_REVERSED = true;

    public static final int BL_DRIVE_MOTOR_PORT = 33;
    public static final int BL_TURN_MOTOR_PORT = 29;
    public static final int BL_TURN_ENCODER_PORT = 2;
    public static final double BL_ENCODER_OFFSET_DEG = 73.0;
    public static final boolean BL_DRIVE_MOTOR_REVERSED = false;
    public static final boolean BL_TURN_MOTOR_REVERSED = false;
    public static final boolean BL_TURN_ENCODER_REVERSED = true;

    public static final int BR_DRIVE_MOTOR_PORT = 11;
    public static final int BR_TURN_MOTOR_PORT = 13;
    public static final int BR_TURN_ENCODER_PORT = 3;
    public static final double BR_ENCODER_OFFSET_DEG = 161.53;
    public static final boolean BR_DRIVE_MOTOR_REVERSED = true;
    public static final boolean BR_TURN_MOTOR_REVERSED = false;
    public static final boolean BR_TURN_ENCODER_REVERSED = true;

    public static final SwerveModuleState[] LOCKED_WHEELS = {
      new SwerveModuleState(0, new Rotation2d(-PI/4)),
      new SwerveModuleState(0, new Rotation2d( PI/4)),
      new SwerveModuleState(0, new Rotation2d( PI/4)),
      new SwerveModuleState(0, new Rotation2d(-PI/4)),
    };
  }

  public static final class DriveConstants {
    private DriveConstants() {}

    // distance between left and right wheels (meters)
    public static final double TRACK_WIDTH = Units.inchesToMeters(18.75);
    // distance between front and back wheels (meters)
    public static final double WHEEL_BASE = Units.inchesToMeters(24.75);
    public static final SwerveDriveKinematics DRIVE_KINEMATICS = new SwerveDriveKinematics(
      // each argument is the location of a swerve module relative to the robot's center
      new Translation2d( WHEEL_BASE / 2, -TRACK_WIDTH / 2),
      new Translation2d( WHEEL_BASE / 2,  TRACK_WIDTH / 2),
      new Translation2d(-WHEEL_BASE / 2, -TRACK_WIDTH / 2),
      new Translation2d(-WHEEL_BASE / 2,  TRACK_WIDTH / 2)
    );

    public static final double MAX_PHYSICAL_SPEED = 3;         // meters per sec
    public static final double MAX_DRIVE_SPEED = 3;            // meters per sec
    public static final double MAX_TURN_SPEED_RAD = PI/1.25;   // rad per sec
    public static final double NO_OUTPUT = 0;                  // meters per sec
    // public static final double MAX_DRIVE_ACCEL    = 1;      // meters per sec^2
    // public static final double MAX_TURN_SPEED     = 360;    // degrees per sec
    // public static final double MAX_TURN_ACCEL     = 180;    // degrees per sec^2

  }
 
  public static final class AutoTaxiConstants {
    private AutoTaxiConstants() {}

    public static final ChassisSpeeds EXIT_COMMUNITY = new ChassisSpeeds(
        0,     // y speed m/s
        -0.5,      // x speed m/s
        0   // theta speed rad/s
    );

    /* field relative
     *         ^ vx, y
     *         | NEG
     *         |
     * <--POS--|--NEG--> vy, x
     *         |
     *         | POS
     *         V
     */

    public static final double EXIT_COMMUNITY_DURATION = 4.5;
  }

  public static final class IntakeConstants {
    private IntakeConstants() {}

    public static final int INTAKE_VICTOR_ID = 5;
    public static final int INTAKE_SPARK_ID = 42;
    // can spark max id is 42

    public static final boolean MOTORS_INVERTED = true;

    public static final double INTAKE_SPEED = -1;
    public static final double NO_OUTPUT = 0;
  }

  public static final class ArmConstants {
    private ArmConstants() {}

    //public static final boolean ARM_OUTPUT_DEBUG_MODE = false;
    public static final int ARM_MOTOR_ID = 6;
    //public static final boolean ARM_MOTOR_INVERTED = true;
    //public static final double MIN_MOTOR_OUTPUT = 0.05;
    public static final double NO_OUTPUT = 0;
    public static final double SPEED = -1;

    public static final int UNSPOOL_TIMER = 120; //in frames (60 per second)
    // public static final int ARM_ENCODER_PORT = 4;
    // public static final double ARM_KP = 0.2;
    // public static final double ARM_KI = 0;
    // public static final double ARM_KD = 0;
  }

  public static final class OIConstants {
    private OIConstants() {}

    public static final boolean OI_DEBUG_MODE = false;
    public static final int DRIVER_JOYSTICK_PORT = 0;
    public static final int COPILOT_JOYSTICK_PORT = 1;

    // public static final double AXIS_DEADBAND = 0.1;
    public static final double AXIS_DEADBAND_PRE = 0.1;
    public static final double AXIS_DEADBAND_POST = 0.35;
    public static final double TURN_DEADBAND = 0.05;
  }

  public static final class TimerConstants {
    private TimerConstants () {}

    public static final double MIN_TIMER_DURATION = 0.02; // sec (code runs at 50 Hz)
  }

  public static final class FieldConstants {
    private FieldConstants() {}

    // DONE: invert constants and set to what everyone else is using
    // also needs to be changed from Pose2d to Pose3d
    public static final double FIELD_LENGTH = Units.feetToMeters(54.271);
    public static final double FIELD_WIDTH  = Units.feetToMeters(26.292);

    //put in an enumerator or array, may make it easier to use
    public static final int RED_SUPPLY_TAG_ID  = 4;
    public static final int RED_LEFT_TAG_ID    = 3;
    public static final int RED_CENTER_TAG_ID  = 2;
    public static final int RED_RIGHT_TAG_ID   = 1;

    public static final int BLUE_SUPPLY_TAG_ID = 5;
    public static final int BLUE_LEFT_TAG_ID   = 6;
    public static final int BLUE_CENTER_TAG_ID = 7;
    public static final int BLUE_RIGHT_TAG_ID  = 8;

    public static final Pose3d RED_SUPPLY_TAG = new Pose3d(
      Units.feetToMeters(53.08),
      Units.feetToMeters(22.145),
      Units.feetToMeters(2.282),
      new Rotation3d(0, 0, Units.degreesToRadians(180))
    );

    public static final Pose3d RED_LEFT_TAG = new Pose3d(
      Units.feetToMeters(50.898),
      Units.feetToMeters(3.516),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0, Units.degreesToRadians(180))
    );

    public static final Pose3d RED_CENTER_TAG = new Pose3d(
      Units.feetToMeters(50.898),
      Units.feetToMeters(9.015),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0, Units.degreesToRadians(180))
    );

    public static final Pose3d RED_RIGHT_TAG = new Pose3d(
      Units.feetToMeters(50.898),
      Units.feetToMeters(14.516),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0, Units.degreesToRadians(180))
    );

    public static final Pose3d BLUE_SUPPLY_TAG = new Pose3d(
      Units.feetToMeters(1.188),
      Units.feetToMeters(22.145),
      Units.feetToMeters(2.282),
      new Rotation3d(0, 0, Units.degreesToRadians(0))
    );

    public static final Pose3d BLUE_LEFT_TAG = new Pose3d(
      Units.feetToMeters(3.371),
      Units.feetToMeters(14.516),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0,
      Units.degreesToRadians(0))
    );

    public static final Pose3d BLUE_CENTER_TAG = new Pose3d(
      Units.feetToMeters(3.371),
      Units.feetToMeters(9.016),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0, Units.degreesToRadians(0))
    );

    public static final Pose3d BLUE_RIGHT_TAG = new Pose3d(
      Units.feetToMeters(3.371),
      Units.feetToMeters(3.516),
      Units.feetToMeters(1.518),
      new Rotation3d(0, 0, Units.degreesToRadians(0))
    );

    public static final double NODE_LEFT_X_OFFSET = Units.feetToMeters(-1.062);
    public static final double NODE_CENTER_X_OFFSET = Units.feetToMeters(-1.062);
    public static final double NODE_RIGHT_X_OFFSET = Units.feetToMeters(1.062);
    public static final double NODE_FRONT_Y_OFFSET = Units.feetToMeters(0.631);
    public static final double NODE_BACK_Y_OFFSET = Units.feetToMeters(2.048);
    public static final double NODE_FRONT_Z_OFFSET = Units.feetToMeters(2.833);
    public static final double NODE_BACK_Z_OFFSET = Units.feetToMeters(3.833);

  }

  public static final class VisionConstants {
    private VisionConstants() {}

    // TODO update with actual values
    public static final Translation3d ROBOT_TO_CAM = new Translation3d(0, 0, 0);

    public static final double TARGET_HEIGHT = 0.0;

    public static final int APRILTAG_PIPELINE = 1;
    public static final int NOTE_PIPELINE = 0;

    public static final String CAMERA_NAME = "VisionCam";

    public static final char PITCH = 'p';
    public static final char YAW = 'y';

    public static final double SLOPPYNES = 0.5;
  }

  public static final class ShooterConstants {
    private ShooterConstants() {}

    public static final int SHOOTER_MOTOR_PORT_1 = 21;
    public static final int SHOOTER_MOTOR_PORT_2 = 24;

    public static final double SHOOTER_SPEED_PORT_1 = 1;
    public static final double SHOOTER_SPEED_PORT_2 = -0.59;
    public static final double NO_OUTPUT = 0.0; //This seems familliar
  }

  public static final class HeadConstants {  
    //private HeadConstants() {} // doing this does nothing positve but does unnessisalily increase the amount of memory used-CF 4662

    public static final int YAW_SERVO = 0;
    public static final int PITCH_SERVO = 1;

    public static final double ANGLE_INCREMENT = 0.01;
  }

}
