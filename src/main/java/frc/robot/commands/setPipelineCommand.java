// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.VisionSubsystem;

public class setPipelineCommand extends Command {
  private final VisionSubsystem visionSubsystem;
  private final Supplier<Integer> pipeline;

  public setPipelineCommand(
    VisionSubsystem visionSubsystem,
    Supplier<Integer> pipeline) {
    this.visionSubsystem = visionSubsystem;
    this.pipeline = pipeline;
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    visionSubsystem.setPipelineIndex(pipeline.get());
  }

  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    return true;
  }
}
