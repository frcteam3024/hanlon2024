// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import static frc.robot.Constants.IntakeConstants.INTAKE_SPEED;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.IntakeSubsystem;

public class IntakeCommand extends Command {
  private final IntakeSubsystem intakeSubsystem;
  private final Supplier<Double> runIntake;

  public IntakeCommand(
    IntakeSubsystem intakeSubsystem,
    Supplier<Double> runIntake
  ) {
    this.intakeSubsystem = intakeSubsystem;
    this.runIntake = runIntake;
    addRequirements(intakeSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    //intakeSubsystem.brakeMode();
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (runIntake.get() >= 0.2) {
      intakeSubsystem.runIntake(INTAKE_SPEED);
    } 
    else if (runIntake.get() <= -0.2){
      intakeSubsystem.runIntake(-INTAKE_SPEED);
    }
    else {
      intakeSubsystem.stopIntake();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    intakeSubsystem.coastMode();
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
