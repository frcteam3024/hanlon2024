// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveSubsystem;

public class GyroReset extends Command {
  private final DriveSubsystem driveSubsystem;
  private final boolean finished = true;
  /** Creates a new GyroReset. */
  public GyroReset(DriveSubsystem driveSubsystem) {
    this.driveSubsystem = driveSubsystem;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    driveSubsystem.resetGyro();
  }

  @Override
  public boolean isFinished() {
    return finished;
  }
}
