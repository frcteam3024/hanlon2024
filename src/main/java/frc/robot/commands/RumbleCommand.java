// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;

public class RumbleCommand extends Command {
  CommandXboxController controller;
  Supplier<Double> rumbleAmount;

  public RumbleCommand(
    CommandXboxController controller,
    Supplier<Double> rumbleAmount
  ) {
    this.controller = controller;
    this.rumbleAmount = rumbleAmount;
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    controller.getHID().setRumble(RumbleType.kBothRumble, rumbleAmount.get());
  }

  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    return false;
  }
}
