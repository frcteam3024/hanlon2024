// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import static frc.robot.Constants.VisionConstants.APRILTAG_PIPELINE;
import static frc.robot.Constants.VisionConstants.SLOPPYNES;
import static frc.robot.Constants.VisionConstants.YAW;

import edu.wpi.first.math.kinematics.ChassisSpeeds;

// import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.commands.SwerveDriveCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class AutoAimCommand extends Command {
  private final VisionSubsystem visionSubsystem;
  private final DriveSubsystem driveSubsystem;
  
  private Boolean finished = false;

  public AutoAimCommand(
    VisionSubsystem visionSubsystem,
    DriveSubsystem driveSubsystem
  ) {
    this.visionSubsystem = visionSubsystem;
    this.driveSubsystem = driveSubsystem;
    addRequirements(driveSubsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (visionSubsystem.rotationAxisToObject(APRILTAG_PIPELINE, YAW) == 999){
      // 0.0
      driveSubsystem.setModules(new ChassisSpeeds(0, 0, 0));
      finished = true;
      System.out.println("No Target");
    }
    if (visionSubsystem.rotationAxisToObject(APRILTAG_PIPELINE, YAW) >= SLOPPYNES) {
      // -0.1
      driveSubsystem.setModules(new ChassisSpeeds(0, 0, -0.35));
    } else if (visionSubsystem.rotationAxisToObject(APRILTAG_PIPELINE, YAW) <= -SLOPPYNES) {
      // 0.1
      driveSubsystem.setModules(new ChassisSpeeds(0, 0, 0.35));
    } else {
      // 0.0
      driveSubsystem.setModules(new ChassisSpeeds(0, 0, 0));
      finished = true;
      System.out.println("AutoAim ended in a aimed state");
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {
    System.out.println("AutoAim was either ended or interupted");
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    System.out.println("AutoAim finished");
    return finished;
  }
}
