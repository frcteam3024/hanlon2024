// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import static frc.robot.Constants.OIConstants.DRIVER_JOYSTICK_PORT;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.commands.RumbleCommand;
import frc.robot.commands.SwerveAutoCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class AutoRoutinePartFour extends SequentialCommandGroup {
  public AutoRoutinePartFour(
    HeadSubsystem headSubsystem,
    DriveSubsystem driveSubsystem,
    VisionSubsystem visionSubsystem,
    IntakeSubsystem intakeSubsystem,
    ShooterSubsystem shooterSubsystem
  ) {
    addCommands(
      new SwerveAutoCommand(
        driveSubsystem,
        new ChassisSpeeds(
          1.0,
          0.0,
          0.0
        ),
        1.0
      ),
      new PickupNoteRoutine(
        headSubsystem,
        driveSubsystem,
        visionSubsystem,
        intakeSubsystem
      ),
      new AutoAimCommand(
        visionSubsystem,
        driveSubsystem
      ),
      new ShootingRoutine(
        shooterSubsystem,
        intakeSubsystem
      ),
      new RumbleCommand(
        new CommandXboxController(DRIVER_JOYSTICK_PORT),
        () -> 1.0
      ),
      new WaitCommand(1.0),
      new RumbleCommand(
        new CommandXboxController(DRIVER_JOYSTICK_PORT), 
        () -> 0.0
      )
    );
  }
}
