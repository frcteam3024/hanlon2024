// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.commands.IntakeCommand;
import frc.robot.commands.ShooterCommand;

public class ShootingRoutine extends SequentialCommandGroup {
  public ShootingRoutine(
    ShooterSubsystem shooterSubsystem,
    IntakeSubsystem intakeSubsystem
  ) {
    addCommands(
      new ShooterCommand(
        shooterSubsystem,
        () -> 0.2,
        () -> 0.0
      ),
      new WaitCommand(1.0),
      new IntakeCommand(
        intakeSubsystem, 
        () -> 0.2
      ),
      new WaitCommand(1.0),
      new IntakeCommand(
        intakeSubsystem, 
        () -> 0.0
      ),
      new ShooterCommand(
        shooterSubsystem, 
        () -> 0.0,
        () -> 0.0
      )
    );
  }
}
