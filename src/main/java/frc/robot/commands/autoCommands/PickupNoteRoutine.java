// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import static frc.robot.Constants.VisionConstants.APRILTAG_PIPELINE;
import static frc.robot.Constants.VisionConstants.NOTE_PIPELINE;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.IntakeCommand;
import frc.robot.commands.SetHeadCommand;
import frc.robot.commands.SwerveAutoCommand;
import frc.robot.commands.setPipelineCommand;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class PickupNoteRoutine extends SequentialCommandGroup {
  public PickupNoteRoutine(
    HeadSubsystem headSubsystem,
    DriveSubsystem driveSubsystem,
    VisionSubsystem visionSubsystem,
    IntakeSubsystem intakeSubsystem
  ) {
    addCommands(
      new SetHeadCommand(
        headSubsystem,
        visionSubsystem,
        () -> 0.0,
        () -> 0.5
      ),
      new setPipelineCommand(
        visionSubsystem,
        () -> NOTE_PIPELINE
      ),
      new WaitCommand(0.5),
      new ConditionalCommand(
        new SequentialCommandGroup(
          new AutoAimCommand(
            visionSubsystem,
            driveSubsystem
          ), 
          new IntakeCommand(
            intakeSubsystem, 
            () -> 1.0
          ),
          new SwerveAutoCommand(
            driveSubsystem,
            new ChassisSpeeds(
              -1.0,
              0.0,
              0.0
            ),
            1.0
          ),
          new IntakeCommand(
            intakeSubsystem, 
            () -> 0.0
          ),
          new SwerveAutoCommand(
            driveSubsystem,
            new ChassisSpeeds(
              1.0,
              0.0,
              0.0
            ),
            1.0
          )
        ),
        new WaitCommand(0.0),
        () -> visionSubsystem.hasTargets()
      ),
      new SetHeadCommand(
        headSubsystem, 
        visionSubsystem,
        () -> 1.0,
        () -> 0.5
      ),
      new setPipelineCommand(
        visionSubsystem,
        () -> APRILTAG_PIPELINE
      )
    );
  }
}
