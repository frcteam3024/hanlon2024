// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import static frc.robot.Constants.AutoTaxiConstants.*;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.ConditionalCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.SetHeadCommand;
import frc.robot.commands.SwerveAutoCommand;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class AutoRoutinePartThree extends SequentialCommandGroup {
  public AutoRoutinePartThree(
    HeadSubsystem headSubsystem,
    VisionSubsystem visionSubsystem,
    DriveSubsystem driveSubsystem,
    ShooterSubsystem shooterSubsystem,
    IntakeSubsystem intakeSubsystem
  ) {
    addCommands(
      new SetHeadCommand(
        headSubsystem,
        visionSubsystem,
        () -> .75,
        () -> .5
      ),
      new WaitCommand(.5),
      new ConditionalCommand(
        new SequentialCommandGroup(
          new AutoAimCommand(
            visionSubsystem, 
            driveSubsystem
          ).repeatedly().withTimeout(1.0),
          new SwerveAutoCommand(
            driveSubsystem,
            new ChassisSpeeds(
              -1.0,
              0.0,
              0.0
            ),
            1.0),
          new ShootingRoutine(
            shooterSubsystem,
            intakeSubsystem
          ),
          new AutoRoutinePartFour(
            headSubsystem,
            driveSubsystem,
            visionSubsystem,
            intakeSubsystem,
            shooterSubsystem
          )
        ),
        /*new AutoRoutinePartFour(
          headSubsystem,
          driveSubsystem,
          visionSubsystem,
          intakeSubsystem,
          shooterSubsystem
        )*/
        new SwerveAutoCommand(
          driveSubsystem, 
          new ChassisSpeeds(
            0.0,
            .5,
            0.0
          ),
          2
        ),
        () -> (visionSubsystem.getAprilID() == 7) || (visionSubsystem.getAprilID() == 4)
      )
    );
  }
}
