// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.GyroReset;
import frc.robot.commands.ShooterCommand;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.VisionSubsystem;

// NOTE:  Consider using this command inline, rather than writing a subclass.  For more
// information, see:
// https://docs.wpilib.org/en/stable/docs/software/commandbased/convenience-features.html
public class NearSpeaker extends SequentialCommandGroup {
  public NearSpeaker(
    VisionSubsystem visionSubsystem,
    DriveSubsystem driveSubsystem,
    ShooterSubsystem shooterSubsystem,
    IntakeSubsystem intakeSubsystem
  ) {
    addCommands(
      new GyroReset(driveSubsystem),
      new AutoAimCommand(
        visionSubsystem, 
        driveSubsystem
      ).repeatedly().withTimeout(2),
      new ShooterCommand(
        //intakeSubsystem,
        shooterSubsystem,
        () -> 1.0,
        () -> 0.0
      ),
      new WaitCommand(5.0),
      new ShooterCommand(
        //intakeSubsystem,
        shooterSubsystem,
        () -> 0.0,
        () -> 0.0
      )
    );
  }
}
