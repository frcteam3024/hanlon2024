// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands.autoCommands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.commands.GyroReset;
import frc.robot.commands.ShooterCommand;
import frc.robot.commands.SwerveAutoCommand;
import frc.robot.commands.SwerveDriveCommand;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class NearAmpOrSource extends SequentialCommandGroup {
  public NearAmpOrSource(
    DriveSubsystem driveSubsystem,
    VisionSubsystem visionSubsystem,
    ShooterSubsystem shooterSubsystem,
    IntakeSubsystem intakeSubsystem
  ) {
    addCommands(
      new GyroReset(driveSubsystem),

      new SwerveAutoCommand(
        driveSubsystem, 
        new ChassisSpeeds(
          0, 
          0, 
          Math.PI/2),
        0.5),
      new AutoAimCommand(
        visionSubsystem, 
        driveSubsystem 
      ).repeatedly().withTimeout(2),
      new ShooterCommand(
        //intakeSubsystem,
        shooterSubsystem,
        () -> 1.0,
        () -> 0.0
      ),
      new WaitCommand(5.0),
      new ShooterCommand(
        //intakeSubsystem,
        shooterSubsystem,
        () -> 0.0,
        () -> 0.0
      )
    );
  }
}
