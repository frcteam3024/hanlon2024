// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import static frc.robot.Constants.ShooterConstants.SHOOTER_SPEED_PORT_1;
import static frc.robot.Constants.ShooterConstants.SHOOTER_SPEED_PORT_2;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
// import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

public class ShooterCommand extends Command {
  private final ShooterSubsystem shooter;
  //private final IntakeSubsystem intakeSubsystem;
  private final Supplier<Double> shooting;
  private final Supplier<Double> antiShooting;

  public ShooterCommand(
      //IntakeSubsystem intakeSubsystem,
      ShooterSubsystem shooter,
      Supplier<Double> shooting,
      Supplier<Double> antiShooting
  ) {
    //this.intakeSubsystem = intakeSubsystem;
    this.shooter = shooter;
    this.shooting = shooting;
    this.antiShooting = antiShooting;
    addRequirements(shooter);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    if (shooting.get() > 0.15 && !(antiShooting.get() > 0.15)) {
      shooter.runShooter(SHOOTER_SPEED_PORT_1 * shooting.get(), SHOOTER_SPEED_PORT_2 * shooting.get());
    }
    else if (antiShooting.get() > 0.15 && !(shooting.get() > 0.15)){
      shooter.runShooter((-SHOOTER_SPEED_PORT_1 / 4) * antiShooting.get(), (-SHOOTER_SPEED_PORT_2 / 4) * antiShooting.get());
    } else {
      shooter.stopShooter();
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return true;
  }
}
