// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class SetHeadCommand extends Command {
  private final HeadSubsystem headSubsystem;
  private final VisionSubsystem visionSubsystem;
  private final Supplier<Double> pitch;
  private final Supplier<Double> yaw;
  
  public SetHeadCommand(
    HeadSubsystem headSubsystem,
    VisionSubsystem visionSubsystem,
    Supplier<Double> pitch,
    Supplier<Double> yaw
  ) {
    this.headSubsystem = headSubsystem;
    this.visionSubsystem = visionSubsystem;
    this.pitch = pitch;
    this.yaw = yaw;
    addRequirements(headSubsystem);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    headSubsystem.setPitch(pitch.get());
    headSubsystem.setYaw(yaw.get());
    if( pitch.get() <= 0.5) {
      visionSubsystem.rotateStream();
    }
  }

  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    return true;
  }
}
