// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import static frc.robot.Constants.DriveConstants.*;
import static frc.robot.Constants.OIConstants.*;
import static java.lang.Math.*;

// import java.util.Arrays;
// import java.util.Collections;
// import java.util.List;
import java.util.function.Supplier;
// import java.util.stream.Collectors;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveSubsystem;

public class SwerveDriveCommand extends Command {
  /** Creates a new SwerveJoystickCommand. */
    private final DriveSubsystem   driveSubsystem;
    private final Supplier<Double>  xSpeedFunction;
    private final Supplier<Double>  ySpeedFunction;
    private final Supplier<Double>  turnSpeedFunction;
    private final Supplier<Boolean> robotOrientedFunction;

    // TODO rate limiters, max speed
    
    // shuffleboard vars
    private double sbOIRawX = 0;
    private double sbOIRawY = 0;
    private double sbOIRawTurn = 0;
    private String sbOIChassisSpeeds = "";

  public SwerveDriveCommand(
      DriveSubsystem driveSubsystem,
      Supplier<Double> xSpeedFunction,
      Supplier<Double> ySpeedFunction,
      Supplier<Double> turnSpeedFunction,
      Supplier<Boolean> robotOrientedFunction
    ) {
    this.driveSubsystem = driveSubsystem;
    this.xSpeedFunction = xSpeedFunction;
    this.ySpeedFunction = ySpeedFunction;
    this.turnSpeedFunction = turnSpeedFunction;
    this.robotOrientedFunction = robotOrientedFunction;
    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    driveSubsystem.brakeMode();
    //shuffleboard();
  }
  
  @Override
  public void execute() {
    // get real-time joystick inputs
    double xSpeed = xSpeedFunction.get();
    double ySpeed = ySpeedFunction.get();
    double turnSpeed = turnSpeedFunction.get();

    // apply deadband
    if (abs(xSpeed) <= AXIS_DEADBAND_PRE) xSpeed = NO_OUTPUT;
    if (abs(ySpeed) <= AXIS_DEADBAND_PRE) ySpeed = NO_OUTPUT;
    if (abs(turnSpeed) <= TURN_DEADBAND) turnSpeed = NO_OUTPUT;

    // cube inputs for better sensitivity
    xSpeed = pow(xSpeed, 3);
    ySpeed = pow(ySpeed, 3);
    turnSpeed = pow(turnSpeed, 3);

    sbOIRawX = xSpeed;
    sbOIRawY = ySpeed;
    sbOIRawTurn = turnSpeed;

    // convert to meters per second
    xSpeed *= MAX_DRIVE_SPEED;
    ySpeed *= MAX_DRIVE_SPEED;
    turnSpeed *= MAX_TURN_SPEED_RAD;

    // construct desired chassis speeds
    ChassisSpeeds chassisSpeeds;
    if (robotOrientedFunction.get().equals(Boolean.TRUE)) {
      // relative to robot
      chassisSpeeds = new ChassisSpeeds(ySpeed, xSpeed, turnSpeed);
    } else {
      // relative to field
      chassisSpeeds = ChassisSpeeds.fromFieldRelativeSpeeds(
          ySpeed, xSpeed, turnSpeed, driveSubsystem.getGyroYawAsRotation2d());
    }

    sbOIChassisSpeeds = chassisSpeeds.toString();

    // output module states to each wheel
    driveSubsystem.setModules(chassisSpeeds, xSpeed, ySpeed);
  }

  @Override
  public void end(boolean interrupted) {
    driveSubsystem.stopMotors();
    driveSubsystem.coastMode();
  }
  
  @Override
  public boolean isFinished() {
    return false;
  }

  public void shuffleboard() {
    ShuffleboardTab sbDebug = Shuffleboard.getTab("Debug");

    sbDebug.addNumber("OI Raw X", () -> sbOIRawX);
    sbDebug.addNumber("OI Raw Y", () -> sbOIRawY);
    sbDebug.addNumber("OI Raw Turn", () -> sbOIRawTurn);
    sbDebug.addString("OI Chassis Speeds", () -> sbOIChassisSpeeds);
  }
}
