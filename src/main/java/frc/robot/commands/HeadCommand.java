package frc.robot.commands;

import static frc.robot.Constants.HeadConstants.ANGLE_INCREMENT;

// import java.lang.Math;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.VisionSubsystem;

public class HeadCommand extends Command {
    private final HeadSubsystem headSubsystem;
    private final VisionSubsystem visionSubsystem;
    private final Supplier<Integer> pov;

    public HeadCommand(
        HeadSubsystem headSubsystem,
        VisionSubsystem visionSubsystem,
        Supplier<Integer> pov
    ) {
        this.headSubsystem = headSubsystem;
        this.visionSubsystem = visionSubsystem;
        this.pov = pov;
        addRequirements(headSubsystem);
    }

    @Override
    public void initialize() {}

    @Override
    public void execute() {
        if (pov.get() != -1) {
            headSubsystem.addToPitch((pov.get() <= 45 || pov.get() >= 315) ? -ANGLE_INCREMENT : 
                                     (pov.get() >= 135 && pov.get() <= 225) ? ANGLE_INCREMENT : 0);
            headSubsystem.addToYaw((pov.get() >= 225 && pov.get() <= 315) ? -ANGLE_INCREMENT : 
                                   (pov.get() >= 45 && pov.get() <= 135) ? ANGLE_INCREMENT : 0);
        }
        if (headSubsystem.getPitch() <= 0.5) {
            //NetworkTableEntry entry = NetworkTableInstance.getDefault().getTable("/Shuffleboard/Main").getEntry("/Input stream");
            visionSubsystem.rotateStream();
        } else {
            visionSubsystem.rotateStreamBack();
        }
    }
    
    @Override
    public void end(boolean interrupted) {}
  
    @Override
    public boolean isFinished() {
      return false;
    }
}
