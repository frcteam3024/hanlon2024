package frc.robot.commands;

import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.DriveSubsystem;

public class SwerveAutoCommand extends Command {
  private final DriveSubsystem driveSubsystem;
  private final ChassisSpeeds desiredMovement;
  private final double duration;
  private final Timer timer;

  public SwerveAutoCommand(
      DriveSubsystem driveSubsystem,
      ChassisSpeeds desiredMovement,
      double duration
  ) {

    this.driveSubsystem = driveSubsystem;
    this.desiredMovement = desiredMovement;
    this.duration = duration;
    this.timer = new Timer();

    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    driveSubsystem.resetGyro();
    driveSubsystem.brakeMode();
    timer.start();
  }

  @Override
  public void execute() {
    driveSubsystem.setModules(desiredMovement);
  }

  @Override
  public void end(boolean interrupted) {
    timer.stop();
    timer.reset();
  }

  @Override
  public boolean isFinished() {
    return timer.get() > duration;
  }
}
