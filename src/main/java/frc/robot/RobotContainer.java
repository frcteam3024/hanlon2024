// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

/*
 * LIST OF AVAILABLE BUTTONS
 * PILOT:
 *  left trigger
 *  b
 *  POV
 *  start
 *  back
 * COPILOT:
 *  right joystick
 *  left joystick
 *  left bumper
 *  left trigger
 *  right trigger
 *  a
 *  b
 *  x
 *  y
 *  start
 *  back
 */

package frc.robot;

import static frc.robot.Constants.OIConstants.*;
import static frc.robot.Constants.AutoTaxiConstants.*;
import static frc.robot.Constants.ModuleConstants.*;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import frc.robot.commands.ArmCommand;
import frc.robot.commands.GyroReset;
import frc.robot.commands.HeadCommand;
import frc.robot.commands.IntakeCommand;
import frc.robot.commands.LockWheels;
import frc.robot.commands.ResetPIDCommand;
import frc.robot.commands.ShooterCommand;
import frc.robot.commands.SwerveAutoCommand;
import frc.robot.commands.SwerveDriveCommand;
import frc.robot.commands.autoCommands.AutoAimCommand;
import frc.robot.commands.autoCommands.AutoRoutine;
import frc.robot.commands.autoCommands.NearAmpOrSource;
import frc.robot.commands.autoCommands.NearSpeaker;
import frc.robot.subsystems.ArmSubsystem;
import frc.robot.subsystems.HeadSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;
import frc.robot.subsystems.DriveSubsystem;
import frc.robot.subsystems.VisionSubsystem;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  private final CommandXboxController copilotController; // We want shooter and intake on this guy
  private final CommandXboxController driverController;

  private final DriveSubsystem driveSubsystem;
  private final ShooterSubsystem shooterSubsystem;
  private final ArmSubsystem armSubsystem;
  private final HeadSubsystem headSubsystem;
  private final IntakeSubsystem intakeSubsystem;
  private final VisionSubsystem visionSubsystem;
  
  private final SwerveDriveCommand swerveDriveCommand;
  private final LockWheels lockWheels;
  private final GyroReset gyroReset;
  private final ShooterCommand shooterCommand;
  private final ArmCommand armCommand;
  private final HeadCommand headCommand;
  private final IntakeCommand intakeCommand;
  private final AutoAimCommand autoAimCommand;
  private final ResetPIDCommand resetPIDCommand;
  
  private final SwerveAutoCommand taxiForDuration;
  private final NearAmpOrSource nearAmpOrSource;
  private final NearSpeaker nearSpeaker;
  private final AutoRoutine autoRoutine;

  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    driverController  = new CommandXboxController(DRIVER_JOYSTICK_PORT);
    copilotController = new CommandXboxController(COPILOT_JOYSTICK_PORT);

    driveSubsystem = new DriveSubsystem();
    shooterSubsystem = new ShooterSubsystem();
    armSubsystem = new ArmSubsystem();
    headSubsystem = new HeadSubsystem();
    intakeSubsystem = new IntakeSubsystem();
    visionSubsystem = new VisionSubsystem();

    swerveDriveCommand = new SwerveDriveCommand(
        driveSubsystem,
        () ->  -driverController.getLeftY(),
        () ->  -driverController.getLeftX(),
        () ->  driverController.getRightX(),
        () ->  driverController.a().getAsBoolean()
      );  

    shooterCommand = new ShooterCommand(
      shooterSubsystem,
      () -> copilotController.getRightTriggerAxis(),
      () -> copilotController.getLeftTriggerAxis()
    );

    armCommand = new ArmCommand(
      armSubsystem, 
      () -> copilotController.getLeftY()
    );

    headCommand = new HeadCommand(
      headSubsystem,
      visionSubsystem,
      () -> copilotController.getHID().getPOV()
    );

    intakeCommand = new IntakeCommand(
      intakeSubsystem,
      () -> copilotController.getRightY()
    );

    autoAimCommand = new AutoAimCommand(
      visionSubsystem, 
      driveSubsystem
    );
    
    resetPIDCommand = new ResetPIDCommand(driveSubsystem);
    lockWheels = new LockWheels(driveSubsystem);
    gyroReset = new GyroReset(driveSubsystem);
    taxiForDuration = new SwerveAutoCommand(driveSubsystem, EXIT_COMMUNITY, EXIT_COMMUNITY_DURATION);
    nearAmpOrSource = new NearAmpOrSource(driveSubsystem, visionSubsystem, shooterSubsystem, intakeSubsystem);
    nearSpeaker = new NearSpeaker(visionSubsystem, driveSubsystem, shooterSubsystem, intakeSubsystem);
    autoRoutine = new AutoRoutine(headSubsystem, visionSubsystem, driveSubsystem, shooterSubsystem, intakeSubsystem);

    driveSubsystem.setDefaultCommand(swerveDriveCommand);
    shooterSubsystem.setDefaultCommand(shooterCommand);
    headSubsystem.setDefaultCommand(headCommand);
    intakeSubsystem.setDefaultCommand(intakeCommand);
    armSubsystem.setDefaultCommand(armCommand);

    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    driverController.b().onTrue(gyroReset);
    driverController.x().onTrue(lockWheels);
    driverController.y().whileTrue(autoAimCommand);
    driverController.leftBumper().onTrue(resetPIDCommand);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   * @return the command to run in autonomous.
   */
  public Command getNullAuto() {
    // uncomment if robot starts near the amp
    /*return new NearAmpOrSource(
      driveSubsystem, 
      visionSubsystem
    );*/

    // uncomment if robot starts near the speaker
    /*return new NearSpeaker(
      visionSubsystem, 
      driveSubsystem, 
      shooterSubsystem
    );*/
    
    return null;
  }
  public Command getTaxiForDuration() {
    return taxiForDuration;
  }

  public Command getAutoIfNearAmpOrSource() {
    return nearAmpOrSource;
  }

  public Command getAutoIfNearSpeaker() {
    return nearSpeaker;
  }

  public Command getAutoRoutine() {
    return autoRoutine;
  }
}
