// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import static frc.robot.Constants.OIConstants.DRIVER_JOYSTICK_PORT;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;

// This is a core function; Gradle will need to be edited if the class name is changed.
public class Robot extends TimedRobot {

  private RobotContainer robotContainer;
  private Command autoCommand;
  private SendableChooser<Command> autoSelector;
  
  @Override
  public void robotInit() {
    robotContainer = new RobotContainer();
    autoSelector = new SendableChooser<>();

    ShuffleboardTab sbMain = Shuffleboard.getTab("Main");
    sbMain
      .add("Autonomous Routine", autoSelector)
      .withWidget(BuiltInWidgets.kComboBoxChooser)
      .withSize(2, 1);
    autoSelector.setDefaultOption("Nothing", robotContainer.getNullAuto());
    autoSelector.addOption("Taxi for Duration", robotContainer.getTaxiForDuration());
    autoSelector.addOption("If Near Amp or Source", robotContainer.getAutoIfNearAmpOrSource());
    autoSelector.addOption("If Near Speaker", robotContainer.getAutoIfNearSpeaker());
    autoSelector.addOption("Auto routine", robotContainer.getAutoRoutine());
  }

  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run();
  }

  @Override
  public void disabledInit() {}

  @Override
  public void disabledPeriodic() {}

  @Override
  public void autonomousInit() {
    autoCommand = autoSelector.getSelected();
    if (autoCommand != null) {
      autoCommand.schedule();
    }
  }

  @Override
  public void autonomousPeriodic() {}

  @Override
  public void teleopInit() {
    if (autoCommand != null) {
      autoCommand.cancel();
    }
  }

  @Override
  public void teleopPeriodic() {}

  @Override
  public void testInit() {
    CommandScheduler.getInstance().cancelAll();
  }

  @Override
  public void testPeriodic() {}

  @Override
  public void simulationInit() {}

  @Override
  public void simulationPeriodic() {}
}
