# Checklist
- [ ] Minimize and reorganize code.
    - [ ] `commands/GyroReset.java`
    - [ ] `commands/LockWheels.java`
    - [ ] `commands/SwerveDriveCommand.java`
    - [x] `commands/TaxiForDuration.java`
    - [ ] `subsystems/GyroSubsystem.java`
    - [ ] `subsystems/SwerveSubsystem.java`
    - [ ] `subsystems/VisionSubsystem.java`
    - [ ] `./Constants.java` (do last)
    - [x] `./Main.java`
    - [x] `./Robot.java`
    - [x] `./RobotContainer.java`
    - [ ] `./SwerveModule.java`
- [ ] Finish `SwerveModule`.
- [ ] Test and finish `VisionSubsystem`.
- [x] Transition from SmartDashboard to Shuffleboard.
    - [ ] Integrate PhotonVision.
- [ ] Make a proper Driver Station.
    - [x] Decide which laptop to use for competition.